#!/usr/bin/env python
import os
import argparse
import logging
import shutil
from contextlib import contextmanager
from pybitbucket.auth import BasicAuthenticator
from pybitbucket.bitbucket import Client
from pybitbucket.repository import (Repository, RepositoryPayload,
                                    RepositoryType, RepositoryForkPolicy)


__doc__ = 'Migrate all bitbucket repositories from user from mercurial to git.'

logger = logging.getLogger(__name__)

DEFAULT_REPOS_DIR = os.path.join(".", "tmp")


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def clone_repository(reposdir, slug):
    repodir = os.path.join(reposdir, slug)
    if os.path.exists(repodir):
        logger.debug("Removing existing dir '%s'" % repodir)
        shutil.rmtree(repodir)

    cmd = "hg clone ssh://hg@bitbucket.org/ferranp/%s %s" % (slug, repodir)
    logger.debug("Running '%s'" % cmd)
    ret = os.system(cmd)
    if ret != 0:
        raise Exception("Error running '%s'" % cmd)


def push_repository(reposdir, slug):
    repodir = os.path.join(reposdir, slug)
    with cd(repodir):
        cmd = "hg push git+ssh://git@bitbucket.org/ferranp/%s.git" % slug
        logger.debug("Running '%s'" % cmd)
        ret = os.system(cmd)
        if ret != 0:
            raise Exception("Error running '%s'" % cmd)


def migrate_repository(bitbucket, reposdir, repo):

    logger.info("Clonning repository '%s'" % repo.slug)
    clone_repository(reposdir, repo.slug)
    # renamen mercurial repository
    name = repo.name

    if not repo.name.endswith("_hg"):
        newname = repo.name + "_hg"
        logger.info("Renaming mercurial repository to '%s'" % newname)
        reponew = repo.put({"name": newname})

    payload = RepositoryPayload({
        'scm': RepositoryType.GIT,
        'name': name,
        'is_private': repo.is_private,
        'description': repo.description,
        'fork_policy': RepositoryForkPolicy.ALLOW_FORKS,
        'language': repo.language,
        'has_issues': repo.has_issues,
        'has_wiki': repo.has_wiki,
    })

    logger.info("Creating new git repository ")
    gitrepo = Repository.create(payload, name, client=bitbucket)

    logger.info("Pushing new git repository ")
    push_repository(reposdir, gitrepo.slug)
    # delete hg repository
    logger.info("Deleting old hg repository '%s'" % reponew.slug)
    reponew.delete()


def main():

    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('-r', '--reposdir', metavar='DIR',
                        default=DEFAULT_REPOS_DIR,
                        help='directory for cloned repositories.')
    parser.add_argument('-u', '--user', dest='user',
                        required=True,
                        help='bitbucket username')
    parser.add_argument('-p', '--password', dest='password',
                        required=True,
                        help='bitbucket password')
    parser.add_argument('-e', '--email', dest='email',
                        required=True,
                        help='bitbucket email')
    parser.add_argument('-d', '--debug', dest='debug',
                        action='store_true',
                        help='Debug messages')

    args = parser.parse_args()

    if args.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    format = '%(asctime)s %(levelname)s %(message)s'
    logging.basicConfig(level=level, format=format)

    bitbucket = Client(
        BasicAuthenticator(
            args.user,
            args.password,
            args.email,
            ))

    for repo in Repository.find_repositories_by_owner_and_role(
                        role='owner',
                        client=bitbucket):

        if repo.scm == 'hg':
            logger.info("Processing repository '%s'" % repo.slug)
            migrate_repository(bitbucket, args.reposdir, repo)


if __name__ == '__main__':

    main()
