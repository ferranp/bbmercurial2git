# bbmercurial2git

Script to convert all yous bitbucket mercurial repositores to git repositories (also on bitbucket).

I needed a tool to convert my 118 mercurial bitbucker repositroies to git before the [bitbucket git subnsetting](https://bitbucket.org/blog/sunsetting-mercurial-support-in-bitbucket). I made this and I converted all my respos in a couple hours.

----
## Requirements

The script ponly needs the [PyBitbucket](https://bitbucket.org/atlassian/python-bitbucket) library installes and mercurial installed wth the [hg-git](https://hg-git.github.io/) extendsion.

----
## usage

    usage: bbmercurial2git.py [-h] [-r DIR] -u USER -p PASSWORD -e EMAIL [-d]

    Migrate all bitbucket repositories from user from mercurial to git.

    optional arguments:
      -h, --help            show this help message and exit
      -r DIR, --reposdir DIR
                            directory for cloned repositories.
      -u USER, --user USER  bitbucket username
      -p PASSWORD, --password PASSWORD
                            bitbucket password
      -e EMAIL, --email EMAIL
                            bitbucket email
      -d, --debug           Debug messages


----
## Limitations

* Does not work with mercurial subrepositories
* If you have .git directories comiited in your mercurial repository, yoy may need to enable *blockdotgit = false* in tyour .hgrc config file.

